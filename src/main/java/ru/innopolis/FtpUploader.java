package ru.innopolis;

/**
 *
 */
public class FtpUploader implements Uploader {
    @Override
    public boolean upload(String content) {
        System.out.println(content + " downloaded from ftp");
        return true;
    }
}
