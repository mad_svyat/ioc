package ru.innopolis;

/**
 *
 */
public interface Uploader {

    boolean upload(String content);
}
