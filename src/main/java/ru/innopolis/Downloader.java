package ru.innopolis;

/**
 *
 */
public interface Downloader {

    String download(String path);
}
