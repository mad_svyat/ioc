package ru.innopolis;

/**
 *
 */
public class HttpUploader implements Uploader {
    @Override
    public boolean upload(String content) {
        System.out.println(content + " downloaded by http");
        return true;
    }
}
